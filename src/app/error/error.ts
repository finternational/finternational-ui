export enum Error {
    Forbidden = 403,
    NotFound = 404
}