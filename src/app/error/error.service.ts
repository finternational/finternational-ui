import { Injectable, OnInit } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { Error } from 'src/app/error/error'

@Injectable()
export class ErrorService {
    public error$ = new BehaviorSubject<Error>(undefined)

    constructor() {}

    public addError(error: number): void {
        this.error$.next(error)
    }

    public clearError() {
        this.error$.next(undefined)
    }
}