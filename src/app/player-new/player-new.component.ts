import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { Subscription } from 'rxjs'
import { PlayerService } from '../player/player.service'
import { Player } from '../player/resources/player'

@Component({
    selector: 'app-player-new',
    templateUrl: './player-new.component.html',
    styleUrls: ['./player-new.component.scss']
})
export class PlayerNewComponent {
    @Input() selectedExternal: boolean = false
    @Input() position: number = 0

    @Output() emptyPlayerClickedEvent: EventEmitter<boolean> = new EventEmitter()
    @Output() removePlayerClickEvent: EventEmitter<Player> = new EventEmitter()

    public playerSubscription: Subscription = undefined
    public player: Player = undefined

    kitPath: string = '/assets/emptykit.png'

    public selected: boolean = false

    constructor(private playerService: PlayerService) { }

    public emptyPlayerClicked() {
        if (!this.selected && !this.selectedExternal && !this.player) {
            this.emptyPlayerClickedEvent.emit(true)

            this.kitPath = '/assets/emptykitselected.png'
            this.selected = false

            this.playerService.publishSearchPosition(this.position)
            this.playerSubscription = this.playerService.pickedPlayer$.subscribe(x => this.playerSubscriptionEventHandler(x))
        }
    }

    public removePlayer() {
        this.removePlayerClickEvent.emit(this.player)

        this.kitPath = '/assets/emptykit.png'
        this.player = undefined
    }

    public playerSubscriptionEventHandler(player: Player) {

        if (player !== undefined) {
            this.playerSubscription.unsubscribe()
            this.playerService.publishPlayer(undefined)

            this.emptyPlayerClickedEvent.emit(false)

            this.player = player
            //ALSO SAVE PLAYER to thing


            this.kitPath = `/assets/${this.player.countryName}.png`

        }
    }
}
