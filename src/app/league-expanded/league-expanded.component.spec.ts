import { ComponentFixture, TestBed } from '@angular/core/testing'

import { LeagueExpandedComponent } from './league-expanded.component'

describe('LeagueExpandedComponent', () => {
    let component: LeagueExpandedComponent
    let fixture: ComponentFixture<LeagueExpandedComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ LeagueExpandedComponent ]
        })
            .compileComponents()
    })

    beforeEach(() => {
        fixture = TestBed.createComponent(LeagueExpandedComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
