import { Component, OnDestroy, OnInit } from '@angular/core'
import { Title } from '@angular/platform-browser'
import { ActivatedRoute, Router } from '@angular/router'
import { Subscription } from 'rxjs'
import { GameweekViewModel } from 'src/app/gameweek/view-models/gameweek.viewmodel'
import { LeagueExpandedViewModel } from 'src/app/league-expanded/view-models/league-expanded.viewmodel'
import { ServiceItem } from 'src/app/shared/models/service-item'
import { Gameweek } from '../gameweek/models/gameweek'
import { LeagueExpandedService } from './league-expanded.service'

@Component({
    selector: 'app-league-expanded',
    templateUrl: './league-expanded.component.html',
    styleUrls: ['./league-expanded.component.scss']
})
export class LeagueExpandedComponent implements OnInit, OnDestroy {

    public leagueSubscription: Subscription = undefined
    public gameweekSubscription: Subscription = undefined

    public loading: boolean
    public league: ServiceItem<LeagueExpandedViewModel> = undefined
    public currentGameweek: Gameweek = undefined
    public gameweeks: ServiceItem<Gameweek[]> = undefined

    public gameweekId: number = undefined
    public leagueId: number = undefined

    public redirectFrom: string = undefined

    constructor(
        private title: Title,
        private leagueService: LeagueExpandedService,
        private activatedRoute: ActivatedRoute,
        private router: Router) {
        this.leagueSubscription = this.leagueService.league$.subscribe(x => this.handleLeagueSubscription(x))
        this.gameweekSubscription = this.leagueService.gameweeks$.subscribe(x => this.handleGameweekSubscription(x))
    }

    public ngOnInit(): void {
        this.title.setTitle('League')
        this.loading = true
        this.leagueId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'))
        this.gameweekId = parseInt(this.activatedRoute.snapshot.paramMap.get('gameweekId'))
        this.redirectFrom = this.activatedRoute.snapshot.queryParams['redirect']

        if (isNaN(this.leagueId)) {
            this.router.navigate(['/notfound'])
        } else {
            this.leagueService.loadLeague(this.leagueId, this.gameweekId)
        }
    }

    public ngOnDestroy(): void {
        this.leagueSubscription.unsubscribe()
        this.gameweekSubscription.unsubscribe()
    }

    public navigateForwardBack(forward: boolean): void {
        this.loading = true

        if (forward) {
            this.gameweekId++
        } else {
            this.gameweekId--
        }

        this.router.navigate([`/league/${this.leagueId}/gameweek/${this.gameweekId}`])
        this.leagueService.loadLeague(this.leagueId, this.gameweekId)
    }

    public dismissNotification(): void {
        this.redirectFrom = undefined
    }

    public handleLeagueSubscription(league: ServiceItem<LeagueExpandedViewModel>): void {
        if (league !== undefined) {

            if (!league.error) {
                this.title.setTitle(`League - ${league.data.name}`)
            }

            this.league = league
            this.loading = false
        }
    }

    public handleGameweekSubscription(gameweeks: ServiceItem<GameweekViewModel[]>): void {
        if (gameweeks !== undefined) {
            this.gameweeks = gameweeks

            if (!gameweeks.error) {
                this.currentGameweek = gameweeks.data.find(x => x.current)
            }
        }
    }

    get canNavigateBack(): boolean {
        return this.gameweekId > this.gameweeks.data.find(x => true)?.id
    }

    get canNavigateForward(): boolean {
        return this.gameweekId < this.gameweeks.data[this.gameweeks.data.length - 1]?.id
    }

    get notificationText(): string {
        return this.redirectFrom === 'create' ? 'League created successfully!' : 'League joined successfully!'
    }
}
