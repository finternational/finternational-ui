import { Injectable, OnInit } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { ApiService } from '../shared/services/api.service'
import config from 'src/config/config.json'
import { LeagueExpandedResponse } from './resources/league-expanded-response'
import { ServiceItem } from 'src/app/shared/models/service-item'
import { LeagueExpandedViewModel } from 'src/app/league-expanded/view-models/league-expanded.viewmodel'
import { GameweekService } from 'src/app/gameweek/gameweek.service'
import { GameweekViewModel } from 'src/app/gameweek/view-models/gameweek.viewmodel'

@Injectable()
export class LeagueExpandedService {
    public gameweeks$ = new BehaviorSubject<ServiceItem<GameweekViewModel[]>>(undefined)
    public league$ = new BehaviorSubject<ServiceItem<LeagueExpandedViewModel>>(undefined)

    constructor(
        private apiService: ApiService,
        private gameweekService: GameweekService) {}

    public loadLeague(id: number, gameweekId: number): void {
        this.gameweekService.get()
            .subscribe((response) => {
                this.gameweeks$.next(new ServiceItem(response, false, undefined))
                this.loadLeagueInternal(id, gameweekId)
            },
            (error: number) => {
                this.gameweeks$.next(new ServiceItem(undefined, true, error))
                this.league$.next(new ServiceItem(undefined, true, error))
            })
    }

    public loadLeagueInternal(id: number, gameweekId: number): void {
        this.apiService.getErrorHandling<LeagueExpandedResponse>(`${config.fantasyApiUrl}/league-expanded/${id}/gameweek/${gameweekId}`)
            .subscribe((response) => {
                this.loadLeagueSuccess(response)
            }, (error) =>{
                this.league$.next(new ServiceItem(undefined, true, error))
            })
    }

    public loadLeagueSuccess(data: LeagueExpandedResponse): void {
        const viewModel = new LeagueExpandedViewModel(data.id, data.name, data.standings)
        this.league$.next(new ServiceItem(viewModel, false, undefined))
    }
}