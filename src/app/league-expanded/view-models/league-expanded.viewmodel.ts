import { Standing } from 'src/app/league-expanded/resources/league-expanded-response'

export class LeagueExpandedViewModel {
    id: number
    name: string
    standings: Standing[]

    constructor(
        id: number,
        name: string,
        standings: Standing[]) {
        this.id = id
        this.name = name
        this.standings = standings
    }
}