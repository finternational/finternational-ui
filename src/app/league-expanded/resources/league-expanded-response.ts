export class LeagueExpandedResponse {
    id: number
    name: string
    standings: Standing[]
}

export class Standing {
    teamId: string
    teamName: string
    firstName: string
    lastName: string
    points: number
    gameweekPoints: number
}