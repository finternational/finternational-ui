import { APP_INITIALIZER, NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { InfoComponent } from './info/info.component'
import { HomeComponent } from './home/home.component'
import { MsalRedirectComponent } from '@azure/msal-angular'
import { NavbarComponent } from './navbar/navbar.component'
import { SharedModule } from 'src/app/shared/shared.module'
import { CommonModule } from '@angular/common'
import { GameweekModule } from './gameweek/gameweek.module'
import { LoaderComponent } from './loader/loader.component'
import { TeamModule } from './team/team.module'
import { PickTeamComponent } from './team/pick-team/pick-team.component'
import { EntryService } from './entry/entry.service'
import { PlayerComponent } from './player/player.component'
import { PickTeamNewComponent } from './pick-team-new/pick-team-new.component'
import { PlayerNewComponent } from './player-new/player-new.component'
import { PlayerPickerComponent } from './player-picker/player-picker.component'
import { FormsModule } from '@angular/forms'
import { PlayerService } from './player/player.service'
import { NotificationService } from './notifications/notification-service'
import { LeagueExpandedComponent } from './league-expanded/league-expanded.component'
import { LeagueExpandedService } from './league-expanded/league-expanded.service'
import { NotFoundComponent } from './not-found/not-found.component'
import { ErrorComponent } from './error/error.component'
import { ErrorService } from './error/error.service'
import { PointsComponent } from './points/points.component'
import { PlayerPointsComponent } from './player-points/player-points.component'
import { GamesComponent } from './games/games.component'
import { GameService } from './games/games.service'
import { GameInfoService } from './game-info/game-info.service'
import { EventComponent } from './event/event.component'
import { PlayerPointsMenuComponent } from './player-points-menu/player-points-menu.component'
import { PerformanceService } from './performance/performance.service'
import { HomeService } from 'src/app/home/service/home.service'
import { PointsService } from 'src/app/points/service/points.service'
import { Title } from '@angular/platform-browser'
import { PickTeamService } from './team/pick-team/pick-team.service'
import { NavbarService } from './navbar/navbar.service'
import { LeagueComponent } from './league/league.component'
import { LeagueJoinComponent } from './league/components/league-join/league-join.component'
import { LeagueCreateComponent } from './league/components/league-create/league-create.component'
import { LeagueService } from './league/league.service'


@NgModule({
    declarations: [
        AppComponent,
        InfoComponent,
        HomeComponent,
        NavbarComponent,
        LoaderComponent,
        PickTeamComponent,
        PlayerComponent,
        PickTeamNewComponent,
        PlayerNewComponent,
        PlayerPickerComponent,
        LeagueExpandedComponent,
        NotFoundComponent,
        ErrorComponent,
        PointsComponent,
        PlayerPointsComponent,
        GamesComponent,
        EventComponent,
        PlayerPointsMenuComponent,
        LeagueComponent,
        LeagueJoinComponent,
        LeagueCreateComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        AppRoutingModule,
        GameweekModule,
        TeamModule,
        FormsModule
    ],
    providers: [
        EntryService,
        PlayerService,
        NotificationService,
        LeagueExpandedService,
        ErrorService,
        GameService,
        GameInfoService,
        PerformanceService,
        HomeService,
        PointsService,
        Title,
        PickTeamService,
        NavbarService,
        LeagueService
    ],
    bootstrap: [AppComponent, MsalRedirectComponent]
})
export class AppModule { }
