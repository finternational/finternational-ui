import { Component, OnDestroy, OnInit } from '@angular/core'
import { AuthService } from 'src/app/shared/services/auth.service'
import { DeviceDetectorService } from 'ngx-device-detector'
import { User } from '../shared/models/User'
import { GameweekService } from 'src/app/gameweek/gameweek.service'
import { Subscription } from 'rxjs'
import { Gameweek } from 'src/app/gameweek/models/gameweek'
import { TeamService } from '../team/team.service'
import { TeamViewModel } from '../team/view-models/team.viewmodel'
import { NavbarService } from './navbar.service'
import { ServiceItem } from '../shared/models/service-item'

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
    public isMobile: boolean = this.deviceService.isMobile()
    public user: User
    public gameweekSubscription: Subscription = undefined
    public teamSubscription: Subscription = undefined

    public nextGameweek: Gameweek
    public currentGameweek: Gameweek

    public gameweeks!: Gameweek[]

    public leagueId!: number

    constructor(
        private authService: AuthService,
        private deviceService: DeviceDetectorService,
        private navbarService: NavbarService) {
        this.user = authService.getUser()

        this.gameweekSubscription = this.navbarService.gameweeks$.subscribe(x => this.handleGameweekSubscription(x))
        this.teamSubscription = this.navbarService.team$.subscribe(x => this.handleTeamSubscription(x))
    }

    public ngOnDestroy(): void {
        this.gameweekSubscription.unsubscribe()
    }

    public ngOnInit(): void {
        this.navbarService.loadGameweeks()
        this.navbarService.loadTeam(this.user.id)
    }

    public logOut(): void {
        this.authService.logout()
    }

    public handleGameweekSubscription(gameweeks: ServiceItem<Gameweek[]>): void {
        if (gameweeks !== undefined) {
            if (!gameweeks.error) {
                this.gameweeks = gameweeks.data
                this.nextGameweek = gameweeks.data.find(x => x.next)
                this.currentGameweek = gameweeks.data.find(x => x.current)
            }
        }
    }

    public handleTeamSubscription(team: ServiceItem<TeamViewModel>): void {
        if (team !== undefined) {
            if (!team.error) {
                this.leagueId = team.data.league
            }
        }
    }

    get gameweekToDisplay(): number {
        return this.currentGameweek?.id ?? this.nextGameweek?.id
    }
}
