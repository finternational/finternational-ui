import { Injectable } from '@angular/core'
import { BehaviorSubject, Subject } from 'rxjs'
import { GameweekService } from 'src/app/gameweek/gameweek.service'
import { GameweekViewModel } from 'src/app/gameweek/view-models/gameweek.viewmodel'
import { ServiceItem } from 'src/app/shared/models/service-item'
import { TeamViewModel } from 'src/app/team/view-models/team.viewmodel'
import { TeamService } from '../team/team.service'

@Injectable()
export class NavbarService {
    public gameweeks$ = new BehaviorSubject<ServiceItem<GameweekViewModel[]>>(undefined)
    public team$ = new BehaviorSubject<ServiceItem<TeamViewModel>>(undefined)

    constructor(
        private gameweekService: GameweekService,
        private teamService: TeamService) {}

    public loadGameweeks(): void {
        this.gameweekService.get()
            .subscribe((response) => {
                this.gameweeks$.next(new ServiceItem(response, false, undefined))
            },
            (error: number) => {
                this.gameweeks$.next(new ServiceItem(undefined, true, error))
            })
    }

    public loadTeam(id: string, reload: boolean = false): void {
        this.teamService.get(id, reload)
            .subscribe((response) => {
                this.team$.next(new ServiceItem(response, false, undefined))
            },
            (error: number) => {
                this.team$.next(new ServiceItem(undefined, true, error))
            })
    }
}