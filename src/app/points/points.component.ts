import { Component, OnDestroy, OnInit } from '@angular/core'
import { Title } from '@angular/platform-browser'
import { ActivatedRoute, Router } from '@angular/router'
import { Subscription } from 'rxjs'
import { Gameweek } from 'src/app/gameweek/models/gameweek'
import { PointsService } from 'src/app/points/service/points.service'
import { ServiceItem } from 'src/app/shared/models/service-item'
import { validate as validGuid } from 'uuid'
import { EntryService } from '../entry/entry.service'
import { Entry } from '../entry/resources/entry'
import { Error } from '../error/error'
import { Player } from '../player/resources/player'

@Component({
    selector: 'app-points',
    templateUrl: './points.component.html',
    styleUrls: ['./points.component.scss']
})
export class PointsComponent implements OnInit, OnDestroy {
    public loading: boolean
    public loadingGameweeks: boolean
    public entry: Entry = undefined
    public gameweeks: ServiceItem<Gameweek[]> = undefined

    public errorText: string = undefined

    public gameweekSubscription: Subscription = undefined
    public entrySubscription: Subscription = undefined
    public errorSubscription: Subscription = undefined

    public showPointsInfoMenu: boolean
    public selectedPlayer: Player
    public gameweekId: number = undefined
    public entryId: string = undefined

    public goalkeeper: Player = undefined
    public defenders: Player[] = undefined
    public midfielders: Player[] = undefined
    public attackers: Player[] = undefined
    public substitutes: Player[] = undefined
    public subGoalkeeper: Player = undefined

    constructor(
        private title: Title,
        private pointsService: PointsService,
        private entryService: EntryService,
        private activatedRoute: ActivatedRoute,
        private router: Router) {
        this.gameweekSubscription = this.pointsService.gameweeks$.subscribe(x => this.handleGameweekSubscription(x))
        this.entrySubscription = this.entryService.entry$.subscribe(x => this.handleEntrySubscription(x))
        this.errorSubscription = this.entryService.error$.subscribe(x => this.handleErrorSubscription(x))
    }

    public ngOnDestroy(): void {
        this.gameweekSubscription.unsubscribe()
        this.entrySubscription.unsubscribe()
        this.errorSubscription.unsubscribe()
        this.entryService.clearEntry()
    }

    public ngOnInit(): void {
        this.showPointsInfoMenu = false
        this.loading = true
        this.loadingGameweeks = true

        this.gameweekId = parseInt(this.activatedRoute.snapshot.paramMap.get('gameweekId'))
        this.entryId = this.activatedRoute.snapshot.paramMap.get('entryId')

        if (isNaN(this.gameweekId) || !validGuid(this.entryId)) {
            this.router.navigate(['/notfound'])
        } else {
            this.pointsService.loadGameweeks()
            this.entryService.load(this.entryId, this.gameweekId)
        }
    }

    public handleGameweekSubscription(item: ServiceItem<Gameweek[]>): void {
        if (item !== undefined) {

            this.gameweeks = item
            this.loadingGameweeks = false
        }
    }

    public handleEntrySubscription(entry: Entry): void {
        if (entry !== undefined) {
            this.title.setTitle(`Points - GW${entry.gameweek}`)

            this.goalkeeper = entry.players.find(x => x.position === 1 && !x.substitute) //move to enum and possibly for one loop
            this.defenders = entry.players.filter(x => x.position === 2 && !x.substitute)
            this.midfielders = entry.players.filter(x => x.position === 3 && !x.substitute)
            this.attackers = entry.players.filter(x => x.position === 4 && !x.substitute)
            this.substitutes = entry.players.filter(x => x.substitute && x.position !== 1)
            this.subGoalkeeper = entry.players.find(x => x.substitute && x.position === 1)
            this.entry = entry
            this.loading = false
        }
    }

    public handleErrorSubscription(error: Error): void {
        if (error !== undefined) {
            this.entryService.errorHandled()

            switch (error) {
            case Error.Forbidden:
                this.errorText = 'You cannot see this entry before the deadline'
                break
            case Error.NotFound:
                this.router.navigate(['/notfound'])
                break
            default:
                this.errorText = 'There was a problem loading this entry'
                break
            }

            this.loading = false
        }
    }

    public showPointsInfoEventHandler(player: Player): void {
        this.selectedPlayer = player
        this.showPointsInfoMenu = true
    }

    public closeEventHandler(): void {
        this.selectedPlayer = undefined
        this.showPointsInfoMenu = false
    }

    public navigateForwardBack(forward: boolean): void {
        this.loading = true

        if (forward) {
            this.gameweekId++
        } else {
            this.gameweekId--
        }

        this.router.navigate([`/entry/${this.entryId}/gameweek/${this.gameweekId}`])
        this.entryService.load(this.entryId, this.gameweekId)
    }

    get points(): number {
        let points = 0
        this.entry.players.forEach(player => {
            if (!player.substitute) {
                points += player.points
            }
        })

        return points
    }

    get teamPicked(): boolean {
        return this.entry?.players?.length > 0
    }

    get canNavigateBack(): boolean {
        return this.gameweekId > this.gameweeks.data.find(x => true)?.id
    }

    get canNavigateForward(): boolean {
        return this.gameweekId < this.gameweeks.data[this.gameweeks.data.length - 1]?.id
    }
}
