import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs'
import { GameweekService } from 'src/app/gameweek/gameweek.service'
import { GameweekViewModel } from 'src/app/gameweek/view-models/gameweek.viewmodel'
import { ServiceItem } from 'src/app/shared/models/service-item'

@Injectable()
export class PointsService {
    public gameweeks$ = new BehaviorSubject<ServiceItem<GameweekViewModel[]>>(undefined)

    constructor(private gameweekService: GameweekService) {}

    public loadGameweeks(): void {
        this.gameweekService.get()
            .subscribe((response) => {
                this.gameweeks$.next(new ServiceItem(response, false, undefined))
            },
            (error: number) => {
                this.gameweeks$.next(new ServiceItem(undefined, true, error))
            })
    }
}