export class Team {
    id: string
    teamName: string
    managerFirstName: string
    managerLastName: string
    league: number
}