import { Injectable, OnInit } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import config from 'src/config/config.json'
import { ServiceItem } from 'src/app/shared/models/service-item'
import { GameweekService } from 'src/app/gameweek/gameweek.service'
import { GameweekViewModel } from 'src/app/gameweek/view-models/gameweek.viewmodel'
import { ApiService } from 'src/app/shared/services/api.service'
import { Entry } from 'src/app/entry/resources/entry'
import { EntryViewModel } from 'src/app/entry/view-models/entry.viewmodel'

@Injectable()
export class PickTeamService {
    public gameweeks$ = new BehaviorSubject<ServiceItem<GameweekViewModel[]>>(undefined)
    public entry$ = new BehaviorSubject<ServiceItem<EntryViewModel>>(undefined)

    constructor(
        private apiService: ApiService,
        private gameweekService: GameweekService) {}

    public loadEntry(teamId: string): void {
        this.gameweekService.get()
            .subscribe((response) => {
                const nextGameweekId = response.find(x => x.next)?.id
                if (nextGameweekId !== undefined) {
                    this.gameweeks$.next(new ServiceItem(response, false, undefined))
                    this.loadEntryInternal(teamId, nextGameweekId)
                } else {
                    this.gameweeks$.next(new ServiceItem(undefined, true, 999))
                    this.entry$.next(new ServiceItem(undefined, true, 999))
                }
            },
            (error: number) => {
                this.gameweeks$.next(new ServiceItem(undefined, true, error))
                this.entry$.next(new ServiceItem(undefined, true, error))
            })
    }

    private loadEntryInternal(teamId: string, gameweekId: number): void {
        this.apiService.getErrorHandling<Entry>(`${config.fantasyApiUrl}/teams/${teamId}/entries/gameweek/${gameweekId}`)
            .subscribe((response) => {
                this.loadEntrySuccess(response)
            }, (error) =>{
                this.entry$.next(new ServiceItem(undefined, true, error))
            })
    }

    private loadEntrySuccess(data: Entry): void {
        const viewModel = new EntryViewModel(data.id, data.players)
        this.entry$.next(new ServiceItem(viewModel, false, undefined))
    }
}