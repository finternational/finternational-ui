import { Component, OnChanges, OnDestroy, OnInit } from '@angular/core'
import { Title } from '@angular/platform-browser'
import { Router } from '@angular/router'
import { Subscription } from 'rxjs'
import { EntryService } from 'src/app/entry/entry.service'
import { EntryViewModel } from 'src/app/entry/view-models/entry.viewmodel'
import { Gameweek } from 'src/app/gameweek/models/gameweek'
import { NotificationService } from 'src/app/notifications/notification-service'
import { MoveSubEvent } from 'src/app/player/resources/move-sub-event'
import { Player } from 'src/app/player/resources/player'
import { ServiceItem } from 'src/app/shared/models/service-item'
import { User } from 'src/app/shared/models/User'
import { AuthService } from 'src/app/shared/services/auth.service'
import { PickTeamService } from './pick-team.service'

@Component({
    selector: 'app-pick-team',
    templateUrl: './pick-team.component.html',
    styleUrls: ['./pick-team.component.scss']
})
export class PickTeamComponent implements OnInit, OnDestroy {

    public loading: boolean
    public gameweekSubscription: Subscription = undefined
    public entrySubscription: Subscription = undefined
    public errorSubscription: Subscription = undefined

    public user: User
    public entry: ServiceItem<EntryViewModel> = undefined
    public nextGameweek: Gameweek

    public goalkeepers: Player[] = undefined //this is a hack to make sure the goalkeeper component is destroyed/newed
    public defenders: Player[] = undefined
    public midfielders: Player[] = undefined
    public attackers: Player[] = undefined
    public substitutes: Player[] = undefined
    public subGoalkeeper: Player[] = undefined

    public transferSelected: boolean = false
    public transferOut: Player = undefined
    public requiredTransferPosition: number = undefined

    public infoShown: boolean = false

    public changes: boolean = false

    public showError: boolean = false
    public showSuccess: boolean = false
    public errorText: string = ''

    constructor(
        private title: Title,
        private pickTeamService: PickTeamService,
        private entryService: EntryService,
        private router: Router,
        private authService: AuthService,
        private notificationService: NotificationService) {
        this.user = authService.getUser()

        this.gameweekSubscription = this.pickTeamService.gameweeks$.subscribe(x => this.handleGameweekSubscription(x))
        this.entrySubscription = this.pickTeamService.entry$.subscribe(x => this.handleEntryViewModelSubscription(x))
        this.errorSubscription = this.notificationService.errorText$.subscribe(x => this.handleErrorSubscription(x))
    }

    public ngOnInit(): void {
        this.setTitle()
        this.loading = true

        this.pickTeamService.loadEntry(this.user.id)
    }

    public ngOnDestroy(): void {
        this.entrySubscription.unsubscribe()
        this.gameweekSubscription.unsubscribe()
        this.errorSubscription.unsubscribe()
        this.entryService.clearEntry()
    }

    public handleGameweekSubscription(item: ServiceItem<Gameweek[]>): void {
        if (item !== undefined) {
            if (!item.error) {
                this.nextGameweek = item.data.find(x => x.next)
            } else if (item.errorCode === 999) {
                this.router.navigate(['/home'])
            }
        }
    }

    public handleEntryViewModelSubscription(item: ServiceItem<EntryViewModel>): void {
        if (item !== undefined) {

            if (!item.error) {
                this.setTitle()
                this.entryChanged(item.data)
            }

            this.entry = item
            this.loading = false
        }
    }

    public handleErrorSubscription(error: string): void {
        this.errorText = error
    }

    public entryChanged(entry: EntryViewModel): void {
        this.goalkeepers = entry.players.filter(x => x.position === 1 && !x.substitute) //move to enum and possibly for one loop
        this.defenders = entry.players.filter(x => x.position === 2 && !x.substitute)
        this.midfielders = entry.players.filter(x => x.position === 3 && !x.substitute)
        this.attackers = entry.players.filter(x => x.position === 4 && !x.substitute)
        this.substitutes = entry.players.filter(x => x.substitute && x.position !== 1)
        this.subGoalkeeper = entry.players.filter(x => x.substitute && x.position === 1)
    }

    public stopTransfer(): void {
        this.transferOut = undefined
        this.requiredTransferPosition = undefined
        this.transferSelected = false
    }

    public transferSelectedEventHandler(player: Player): void {
        if (!player.substitute && this.transferOut !== undefined) {
            this.stopTransfer()
        } else if (!player.substitute && this.transferOut === undefined) {
            this.transferOut = player
            this.setRequiredTransferPosition(player.position)
            this.transferSelected = true
        } else if (player.substitute && this.transferOut !== undefined) {
            const playerOut = this.entry.data.players.findIndex(x => x.id === this.transferOut.id)
            const playerIn = this.entry.data.players.findIndex(x => x.id === player.id)

            this.transferOut.substitute = true
            player.substitute = false

            this.entry.data.players[playerOut] = player
            this.entry.data.players[playerIn] = this.transferOut

            this.changes = true
            this.entryChanged(this.entry.data)
            this.stopTransfer()
        }
    }

    public setRequiredTransferPosition(position: number) {
        switch (position) {
        case 1:
            this.requiredTransferPosition = position
            break
        case 2:
            if (this.defenders.length === 3) {
                this.requiredTransferPosition = position
            }
            break
        case 3:
            if (this.midfielders.length === 3) {
                this.requiredTransferPosition = position
            }
            break
        case 4:
            if (this.attackers.length === 1) {
                this.requiredTransferPosition = position
            }
            break
        }
    }

    public showInfoChangedEvent(infoShown: boolean) {
        this.infoShown = infoShown
        this.stopTransfer()
    }

    public changeCaptainEventHandler(player: Player) {
        const oldCaptain = this.entry.data.players.findIndex(x => x.captain)
        const newCaptain = this.entry.data.players.findIndex(x => x.id === player.id)

        this.entry.data.players[oldCaptain].captain = false
        this.entry.data.players[newCaptain].captain = true

        this.changes = true
        this.infoShown = false
    }

    public changeViceCaptainEventHandler(player: Player): void {
        const oldViceCaptain = this.entry.data.players.findIndex(x => x.viceCaptain)
        const newViceCaptain = this.entry.data.players.findIndex(x => x.id === player.id)

        this.entry.data.players[oldViceCaptain].viceCaptain = false
        this.entry.data.players[newViceCaptain].viceCaptain = true

        this.changes = true
        this.infoShown = false
    }

    public moveSubEventHandler(moveSubEvent: MoveSubEvent): void {
        const fromSubIndex = this.substitutes.findIndex(x => x.id === moveSubEvent.player.id)
        const tempPlayer = this.substitutes[fromSubIndex + (moveSubEvent.direction === 0 ? -1 : 1)]

        const fromEntryViewModelIndex = this.entry.data.players.findIndex(x => x.id === moveSubEvent.player.id)
        const toEntryViewModelIndex = this.entry.data.players.findIndex(x => x.id === tempPlayer.id)

        this.entry.data.players[fromEntryViewModelIndex] = tempPlayer
        this.entry.data.players[toEntryViewModelIndex] = moveSubEvent.player

        this.changes = true
        this.entryChanged(this.entry.data)
    }

    public saveChanges(): void {
        this.stopTransfer()
        this.loading = true
        this.showError = false
        this.infoShown = false

        for (let i = 0; i < 15; i++) {
            this.entry.data.players[i].squadPosition = i+1
        }

        this.entryService.update(this.user.id, this.entry.data).subscribe(() => {
            this.loading = false
            this.changes = false
            this.showSuccess = true
        }, () => {
            this.loading = false
            this.showError = true
        })
    }

    public dismissError(): void {
        this.showError = false
    }

    public dismissSuccess(): void {
        this.showSuccess = false
    }

    public dismissErrorText(): void {
        this.notificationService.addError('')
    }

    private setTitle(): void {
        const gameweekText = this.nextGameweek === undefined ? '' : ` - GW${this.nextGameweek.id}`
        this.title.setTitle(`Pick Team${gameweekText}`)
    }
}
