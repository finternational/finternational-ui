import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable, Subscriber } from 'rxjs'
import { ApiService } from '../shared/services/api.service'
import { Team } from './models/team'
import config from 'src/config/config.json'
import { Error } from '../error/error'
import { TeamViewModel } from './view-models/team.viewmodel'

@Injectable()
export class TeamService {
    public team$ = new BehaviorSubject<Team>(undefined)
    public error$ = new BehaviorSubject<Error>(undefined)

    private _team: TeamViewModel = undefined

    constructor(private apiService: ApiService) {
    }

    public get(id: string, reload: boolean = false): Observable<TeamViewModel> {
        return new Observable(observer => {
            if (this._team === undefined || reload) {
                this.loadTeam(observer, id)
            } else {
                observer.next(this._team)
                observer.complete()
            }
        })
    }

    private loadTeam(observer: Subscriber<TeamViewModel>, id: string): void {
        this.apiService.getErrorHandling<Team>(`${config.fantasyApiUrl}/teams/${id}`)
            .subscribe((response) => {
                this.loadTeamComplete(observer, response)
            },(error) => {
                observer.error(error)
            })
    }

    private loadTeamComplete(observer: Subscriber<TeamViewModel>, data: Team): void {
        this._team = new TeamViewModel(
            data.id,
            data.teamName,
            data.managerFirstName,
            data.managerLastName,
            data.league)

        observer.next(this._team)
        observer.complete()
    }
}