export class TeamViewModel {
    id: string
    teamName: string
    managerFirstName: string
    managerSecondName: string
    league: number

    constructor(
        id: string,
        teamName: string,
        managerFirstName: string,
        managerSecondName: string,
        league: number) {
        this.id = id
        this.teamName = teamName
        this.managerSecondName = managerSecondName
        this.managerFirstName = managerFirstName
        this.league = league ?? undefined
    }
}