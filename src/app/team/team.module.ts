import { NgModule } from '@angular/core'
import { TeamService } from './team.service'

@NgModule({
    declarations: [],
    imports: [],
    providers:[
        TeamService
    ]
})

export class TeamModule { }