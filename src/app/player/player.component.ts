import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core'
import { DeviceDetectorService } from 'ngx-device-detector'
import { Role } from '../role/role'
import { MoveSubEvent } from './resources/move-sub-event'
import { Player } from './resources/player'

@Component({
    selector: 'app-player',
    templateUrl: './player.component.html',
    styleUrls: ['./player.component.scss']
})

export class PlayerComponent implements OnInit, OnChanges {

    @Input() player: Player = undefined
    @Input() transfer: boolean = false
    @Input() requiredTransferPosition: number = undefined
    @Input() showInfoExternal: boolean = false
    @Input() subPosition: number = undefined

    @Output() transferredPlayer: EventEmitter<Player> = new EventEmitter()
    @Output() showInfoChanged: EventEmitter<boolean> = new EventEmitter()
    @Output() makeCaptain: EventEmitter<Player> = new EventEmitter() //refactor into one
    @Output() makeViceCaptain: EventEmitter<Player> = new EventEmitter()
    @Output() moveSubstitute: EventEmitter<MoveSubEvent> = new EventEmitter()

    public captainText: string = ''
    public selected: boolean = false
    public kitPath: string = '/assets/england.png'
    public canBeTransferred: boolean = true
    public showInfoInternal: boolean = false
    public isMobile: boolean = undefined

    constructor(private deviceService: DeviceDetectorService) { }

    public ngOnInit(): void {

        this.isMobile = this.deviceService.isMobile()

        this.kitPath = `/assets/${this.player.countryName}.png`

        if (this.isMobile) {
            this.kitPath = `/assets/${this.player.countryName}50.png`
        }

        if (this.player.captain) {
            this.captainText = 'C'
        }

        if (this.player.viceCaptain) {
            this.captainText = 'V'
        }
    }

    public ngOnChanges(): void {
        this.canBeTransferred = true

        if (this.player.position === 1 && this.requiredTransferPosition !== 1) {
            this.canBeTransferred = false
        }


        if (this.requiredTransferPosition !== undefined) {
            if (this.player.position !== this.requiredTransferPosition) {
                this.canBeTransferred = false
            }
        }
    }

    public playerSelected(): void {
        this.selected = !this.selected
        this.transferredPlayer.emit(this.player)
    }

    public showInfoClicked(): void {
        this.showInfoInternal = !this.showInfoInternal
        this.selected = false

        this.showInfoChanged.emit(this.showInfoInternal)
    }

    public changeSquadRole(role: Role) {
        if (role === Role.Captain) {
            this.makeCaptain.emit(this.player)
        } else {
            this.makeViceCaptain.emit(this.player)
        }

        this.showInfoInternal = false
    }

    public moveSub(direction: number): void {
        this.moveSubstitute.emit(new MoveSubEvent(this.player, direction))
    }

    get playerDivId(): string {
        return this.player.countryName + this.player.id
    }
}
