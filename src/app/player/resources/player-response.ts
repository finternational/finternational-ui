export class PlayerResponse {
    id: number
    firstName: string
    lastName: string
    position: number
    country: Country
}

export class Country {
    id: number
    name: string
}