import { Player } from './player'

export class MoveSubEvent {
    player: Player
    direction: number

    constructor(player: Player, direction: number) {
        this.player = player
        this.direction = direction
    }
}
