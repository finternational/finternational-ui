export class Player {
    id: number
    firstName: string
    lastName: string
    captain: boolean
    viceCaptain: boolean
    countryName: string
    countryId: number
    substitute: boolean
    position: number
    opponent: string
    points: number
    squadPosition: number

    constructor(id: number, lastName: string, countryName: string, countryId: number, position: number, squadPosition: number) {
        this.id = id
        this.lastName = lastName
        this.countryName = countryName
        this.countryId = countryId
        this.position = position
        this.squadPosition = squadPosition
    }
}