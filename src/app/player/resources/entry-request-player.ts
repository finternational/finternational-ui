export class EntryRequestPlayer {
    playerId: number
    captain: boolean
    viceCaptain: boolean
    substitute: boolean
    squadPosition: number

    constructor(playerId: number, captain: boolean, viceCaptain: boolean, substitute: boolean, squadPosition: number) {
        this.playerId = playerId
        this.captain = captain
        this.viceCaptain = viceCaptain
        this.substitute = substitute
        this.squadPosition = squadPosition
    }
}