import { Injectable, OnInit } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { ApiService } from '../shared/services/api.service'
import config from 'src/config/config.json'
import { PlayerResponse } from './resources/player-response'
import { Player } from './resources/player'

@Injectable()
export class PlayerService {
    public players$ = new BehaviorSubject<PlayerResponse[]>(undefined)
    public pickedPlayer$ = new BehaviorSubject<Player>(undefined)
    public searchPosition$ = new BehaviorSubject<number>(0)

    constructor(private apiService: ApiService) {}

    load(): void {
        this.apiService.get<PlayerResponse[]>(`${config.fantasyApiUrl}/players`).subscribe((response) => {
            this.players$.next(response)
        }) //TODO: add error handling
    }

    public publishPlayer(player: Player) {
        this.pickedPlayer$.next(player)
    }

    public publishSearchPosition(searchPosition: number) {
        this.searchPosition$.next(searchPosition)
    }
}