import { EntryRequestPlayer } from 'src/app/player/resources/entry-request-player'
import { EntryViewModel } from '../view-models/entry.viewmodel'

export class EntryRequest {
    players: EntryRequestPlayer[]

    constructor(entry: EntryViewModel) {
        this.players = []

        entry.players.forEach(player => {
            this.players.push(new EntryRequestPlayer(player.id, player.captain, player.viceCaptain, player.substitute, player.squadPosition))
        })
    }
}
