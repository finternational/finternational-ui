import { Player } from 'src/app/player/resources/player'

export class Entry {
    id: number
    current: string
    gameweek: number
    teamName: string
    managerFirstName: string
    managerLastName: string
    players: Player[]

    constructor() {
        this.players = []
    }
}