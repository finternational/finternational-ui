import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { ApiService } from '../shared/services/api.service'
import { Entry } from 'src/app/entry/resources/entry'
import config from 'src/config/config.json'
import { EntryRequest } from './resources/entry-request'
import { Error } from '../error/error'
import { EntryViewModel } from './view-models/entry.viewmodel'

@Injectable()
export class EntryService {
    public entry$ = new BehaviorSubject<Entry>(undefined)
    public error$ = new BehaviorSubject<Error>(undefined)

    constructor(private apiService: ApiService) {}

    public load(teamId: string, gameweekId: number): void {
        this.apiService.get<Entry>(`${config.fantasyApiUrl}/teams/${teamId}/entries/gameweek/${gameweekId}`).subscribe((response) => {

            //handle 204 no content

            this.entry$.next(response)
        },(error) =>{
            this.error$.next(error.status)
        })
    }

    public errorHandled() {
        this.error$.next(undefined)
    }

    public clearEntry() {
        this.entry$.next(undefined)
    }

    public update(teamId: string, entry: EntryViewModel): Observable<any> {
        const request = new EntryRequest(entry)

        return this.apiService.patch(`${config.fantasyApiUrl}/teams/${teamId}/entries/${entry.id}`, request)
    }
}