import { Player } from 'src/app/player/resources/player'

export class EntryViewModel {
    id: number
    players: Player[]

    constructor(
        id: number,
        players: Player[]
    ) {
        this.id = id
        this.players = players
    }
}