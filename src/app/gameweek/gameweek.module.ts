import { NgModule } from '@angular/core'
import { GameweekService } from './gameweek.service'

@NgModule({
    declarations: [],
    imports: [],
    providers:[
        GameweekService
    ]
})

export class GameweekModule { }