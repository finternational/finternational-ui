export class GameweekViewModel {
    id: number
    current: boolean
    deadline: Date
    end: Date
    next: boolean

    constructor(
        id: number,
        current: boolean,
        deadline: Date,
        end: Date,
        next: boolean) {
        this.id = id
        this.current = current
        this.deadline = deadline
        this.end = end
        this.next = next
    }
}

