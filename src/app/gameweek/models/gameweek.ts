export class Gameweek {
    id: number
    current: boolean
    deadline: Date
    end: Date
    next: boolean
}