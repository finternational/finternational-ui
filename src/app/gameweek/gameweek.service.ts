import { Injectable, OnInit } from '@angular/core'
import { BehaviorSubject, Observable, Subscriber } from 'rxjs'
import { ApiService } from '../shared/services/api.service'
import { Gameweek } from './models/gameweek'
import config from 'src/config/config.json'
import { Error } from '../error/error'
import { GameweekViewModel } from 'src/app/gameweek/view-models/gameweek.viewmodel'

@Injectable()
export class GameweekService {
    public gameweeks$ = new BehaviorSubject<Gameweek[]>(undefined)
    public error$ = new BehaviorSubject<Error>(undefined)

    private _gameweeks: GameweekViewModel[] = undefined

    constructor(private apiService: ApiService) {
    }

    public get(): Observable<GameweekViewModel[]> {
        return new Observable(observer => {
            if (this._gameweeks === undefined) {
                this.loadGameweeks(observer)
            } else {
                observer.next(this._gameweeks)
                observer.complete()
            }
        })
    }

    load(): void {
        this.apiService.get<Gameweek[]>(`${config.fantasyApiUrl}/gameweeks`).subscribe((response) => {
            this.gameweeks$.next(response)
        },(error) =>{
            this.error$.next(error.status)
        })
    }

    public loadGameweeks(observer: Subscriber<GameweekViewModel[]>): void {
        this.apiService.getErrorHandling<Gameweek[]>(`${config.fantasyApiUrl}/gameweeks`)
            .subscribe((response) => {
                this.loadGameweeksComplete(observer, response)
            },(error) => {
                observer.error(error)
            })
    }

    public loadGameweeksComplete(observer: Subscriber<GameweekViewModel[]>, data: Gameweek[]): void {
        const viewModels = data.map(x => new GameweekViewModel(x.id, x.current, x.deadline, x.end, x.next))
        this._gameweeks = viewModels

        observer.next(viewModels)
        observer.complete()
    }

    public errorHandled() {
        this.error$.next(undefined)
    }

    public clearGameweeks() {
        this.gameweeks$.next(undefined)
    }
}