export class CreateLeagueResponse {
    id: number
    name: string
    isPrivate: boolean
    leagueType: number
    joinCode: string
    creator: string
    playersPicked: boolean

    constructor(
        id: number,
        name: string,
        isPrivate: boolean,
        leagueType: number,
        joinCode: string,
        creator: string,
        playersPicked: boolean) {
        this.id = id
        this.name = name
        this.isPrivate = isPrivate
        this.leagueType = leagueType
        this.joinCode = joinCode
        this.creator = creator
        this.playersPicked = playersPicked
    }
}