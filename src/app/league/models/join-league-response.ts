export class JoinLeagueResponse {
    id: number

    constructor(
        id: number) {
        this.id = id
    }
}