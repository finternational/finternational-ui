export class JoinLeagueRequest {
    joinCode: string
  
    constructor(joinCode: string) {
        this.joinCode = joinCode
    }
}