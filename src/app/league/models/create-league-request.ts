export class CreateLeagueRequest {
    name: string
    private: boolean
    leagueType: number

    constructor(name: string, leaguePrivate: boolean, leagueType: number) {
        this.name = name
        this.private = leaguePrivate
        this.leagueType = leagueType
    }
}