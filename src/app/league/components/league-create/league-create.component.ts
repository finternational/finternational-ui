import { Component, EventEmitter, Input, Output } from '@angular/core'
import { Router } from '@angular/router'
import { LeagueService } from 'src/app/league/league.service'
import { NavbarService } from 'src/app/navbar/navbar.service'

@Component({
    selector: 'app-league-create',
    templateUrl: './league-create.component.html',
    styleUrls: ['./league-create.component.scss']
})
export class LeagueCreateComponent {
    @Input() gameweekRedirectId: number
    @Input() teamId: string

    public leagueName: string
    public leaguePrivate: boolean = false
    public leagueType: number = 1
    public loading: boolean = false
    public showError: boolean = false

    @Output() closeCreate: EventEmitter<void> = new EventEmitter()
    constructor(
        private router: Router,
        private leagueService: LeagueService,
        public navbarService: NavbarService) { }

    public closeForm(): void {
        this.closeCreate.emit()
    }

    public leagueNameChange(): void {
        const input = document.getElementById('leagueName')

        input.classList.remove('is-valid')
        input.classList.remove('is-invalid')

        if (this.leagueName.length > 0 && this.leagueName.length <= 50) {
            input.classList.add('is-valid')
        } else {
            input.classList.add('is-invalid')
        }
    }

    public create(): void {
        this.loading = true
        this.showError = false
        this.leagueService.createLeague(this.leagueName, this.leaguePrivate, this.leagueType)
            .subscribe((response) => {
                this.navbarService.loadTeam(this.teamId, true)
                this.router.navigate([`/league/${response.id}/gameweek/${this.gameweekRedirectId}`], { queryParams: { redirect: 'create' } })
            },() =>{
                this.loading = false
                this.showError = true
            })
    }

    public dismissError(): void {
        this.showError = false
    }

    get disableCreate(): boolean {
        return (this.leagueName?.length ?? 0) === 0 || (this.leagueName?.length ?? 0) > 50
    }
}
