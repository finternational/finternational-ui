import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core'
import { Router } from '@angular/router'
import { LeagueService } from 'src/app/league/league.service'
import { NavbarService } from 'src/app/navbar/navbar.service'
import { validate as validGuid } from 'uuid'

@Component({
    selector: 'app-league-join',
    templateUrl: './league-join.component.html',
    styleUrls: ['./league-join.component.scss']
})
export class LeagueJoinComponent {
    @Input() gameweekRedirectId: number
    @Input() teamId: string

    public loading: boolean = false
    public showError: boolean = false
    public joinCode: string
    public errorText: string
    public errorCode: number

    @Output() closeJoin: EventEmitter<void> = new EventEmitter()

    constructor(
        private leagueService: LeagueService,
        public navbarService: NavbarService,
        private router: Router) {

         }

    public closeForm(): void {
        this.closeJoin.emit()
    }


    public joinCodeChange(): void {
        this.errorCode = undefined
        const input = document.getElementById('joinCode')

        input.classList.remove('is-valid')
        input.classList.remove('is-invalid')

        if (this.joinCode.length === 36 && validGuid(this.joinCode)) {
            input.classList.add('is-valid')
        } else {
            input.classList.add('is-invalid')
        }
    }

    public join(): void {
        this.errorCode = undefined
        this.loading = true
        this.showError = false
        this.leagueService.joinLeague(this.joinCode)
            .subscribe((response) => {
                this.navbarService.loadTeam(this.teamId, true)
                this.router.navigate(
                    [`/league/${response.id}/gameweek/${this.gameweekRedirectId}`], 
                    { queryParams: { redirect: 'join' } })
            },(error) =>{
                this.showError = true
                this.errorCode = error
                if (error === 404) {
                    this.errorText = 'League not found.'
                } else {
                    this.errorText = 'Failed to join League, please try again.'
                }
                this.loading = false
            })
    }

    public dismissError(): void {
        this.showError = false
    }

    get disableJoin(): boolean {
        return ((this.joinCode?.length ?? 0) !== 36 && !validGuid(this.joinCode)) || this.errorCode === 404
    }
}