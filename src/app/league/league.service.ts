import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { GameweekService } from 'src/app/gameweek/gameweek.service'
import { ServiceItem } from 'src/app/shared/models/service-item'
import { ApiService } from 'src/app/shared/services/api.service'
import { TeamViewModel } from 'src/app/team/view-models/team.viewmodel'
import { TeamService } from '../team/team.service'
import config from 'src/config/config.json'
import { CreateLeagueRequest } from 'src/app/league/models/create-league-request'
import { CreateLeagueResponse } from 'src/app/league/models/create-league-response'
import { JoinLeagueResponse } from 'src/app/league/models/join-league-response'
import { JoinLeagueRequest } from 'src/app/league/models/join-league.request'

@Injectable()
export class LeagueService {
    public gameweekRedirectId$ = new BehaviorSubject<ServiceItem<number>>(undefined)
    public team$ = new BehaviorSubject<ServiceItem<TeamViewModel>>(undefined)

    constructor(
        private apiService: ApiService,
        private gameweekService: GameweekService,
        private teamService: TeamService) {}

    public loadTeam(id: string): void {
        this.gameweekService.get()
            .subscribe((response) => {
                const nextGameweek = response.find(x => x.next)
                const currentGameweek = response.find(x => x.current)
                const gameweekId = currentGameweek?.id ?? nextGameweek?.id

                if (gameweekId !== undefined) {
                    this.gameweekRedirectId$.next(new ServiceItem(gameweekId, false, undefined))
                    this.loadTeamInternal(id)
                } else {
                    this.gameweekRedirectId$.next(new ServiceItem(undefined, true, 998)) //no next or current gw error move to enum
                    this.team$.next(new ServiceItem(undefined, true, 998))
                }
            },
            (error: number) => {
                this.gameweekRedirectId$.next(new ServiceItem(undefined, true, error))
                this.team$.next(new ServiceItem(undefined, true, error))
            })
    }

    public createLeague(name: string, leaguePrivate: boolean, type: number): Observable<CreateLeagueResponse> {
        const request = new CreateLeagueRequest(name, leaguePrivate, type)
        return this.apiService.post(`${config.fantasyApiUrl}/league`, request)
    }

    public joinLeague(joinCode: string): Observable<JoinLeagueResponse> {
        const request = new JoinLeagueRequest(joinCode)
        return this.apiService.post(`${config.fantasyApiUrl}/league/join`, request) 
    }

    private loadTeamInternal(id: string) : void {
        this.teamService.get(id)
            .subscribe((response) => {
                this.team$.next(new ServiceItem(response, false, undefined))
            },
            (error: number) => {
                this.team$.next(new ServiceItem(undefined, true, error))
            })
    }
}