import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Subscription } from 'rxjs'
import { ServiceItem } from '../shared/models/service-item'
import { AuthService } from '../shared/services/auth.service'
import { TeamViewModel } from '../team/view-models/team.viewmodel'
import { LeagueService } from './league.service'

@Component({
    selector: 'app-league',
    templateUrl: './league.component.html',
    styleUrls: ['./league.component.scss']
})
export class LeagueComponent implements OnInit {
    public gameweekSubscription: Subscription = undefined
    public teamSubscription: Subscription = undefined

    public gameweekRedirectId!: ServiceItem<number>
    public team: ServiceItem<TeamViewModel>

    public showJoin: boolean = false
    public showCreate: boolean = false

    public loading: boolean

    constructor(
        private router: Router,
        private authService: AuthService,
        private leagueService: LeagueService
    ) {
        this.gameweekSubscription = this.leagueService.gameweekRedirectId$.subscribe(x => this.handleGameweekSubscription(x))
        this.teamSubscription = this.leagueService.team$.subscribe(x => this.handleTeamSubscription(x))
    }

    public ngOnInit(): void {
        this.loading = true
        this.leagueService.loadTeam(this.authService.getUser().id)
    }

    public handleGameweekSubscription(gameweekRedirectId: ServiceItem<number>): void {
        if (gameweekRedirectId !== undefined) {
            this.gameweekRedirectId = gameweekRedirectId
        }
    }

    public handleTeamSubscription(team: ServiceItem<TeamViewModel>): void {
        if (team !== undefined) {
            if (!team.error) {
                //handle 998 error as some sort of 'No data yet'?
                this.team = team
                if (team.data.league !== undefined) {
                    this.router.navigate([`/league/${team.data.league}/gameweek/${this.gameweekRedirectId.data}`])
                }
            }

            this.loading = false
        }
    }

    public joinClicked(): void {
        this.showJoin = true
    }

    public createClicked(): void {
        this.showCreate = true
    }

    public closeForm(): void {
        this.showCreate = false
        this.showJoin = false
    }

    get showOuter(): boolean {
        return !this.showJoin && !this.showCreate
    }
}
