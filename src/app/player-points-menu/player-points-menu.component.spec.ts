import { ComponentFixture, TestBed } from '@angular/core/testing'

import { PlayerPointsMenuComponent } from './player-points-menu.component'

describe('PlayerPointsMenuComponent', () => {
    let component: PlayerPointsMenuComponent
    let fixture: ComponentFixture<PlayerPointsMenuComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ PlayerPointsMenuComponent ]
        })
            .compileComponents()
    })

    beforeEach(() => {
        fixture = TestBed.createComponent(PlayerPointsMenuComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
