import { Component, EventEmitter, OnInit, Output, Input, OnDestroy, OnChanges, SimpleChanges } from '@angular/core'
import { Subscription } from 'rxjs'
import { Error } from '../error/error'
import { Performance } from '../performance/performance'
import { PerformanceService } from '../performance/performance.service'
import { Player } from '../player/resources/player'

@Component({
    selector: 'app-player-points-menu',
    templateUrl: './player-points-menu.component.html',
    styleUrls: ['./player-points-menu.component.scss']
})
export class PlayerPointsMenuComponent implements OnInit, OnDestroy, OnChanges {
    @Input() player: Player
    @Output() closeEvent: EventEmitter<void> = new EventEmitter<void>()

    public performances: Performance[]

    public performanceSubscription: Subscription = undefined
    public performanceErrorSubscription: Subscription = undefined

    public errorText: string = undefined
    public loading: boolean

    constructor(private performanceService: PerformanceService) { }

    public ngOnInit(): void {
        this.performanceSubscription = this.performanceService.performance$.subscribe(x => this.handlePerformanceSubscription(x))
        this.performanceErrorSubscription = this.performanceService.error$.subscribe(x => this.handlePerformanceErrorSubscription(x))

        this.loading = true
        this.performanceService.load(this.player.id)
    }

    public ngOnChanges(): void {
        this.loading = true
        this.performanceService.load(this.player.id)
    }

    public ngOnDestroy(): void {
        this.performanceSubscription.unsubscribe()
        this.performanceErrorSubscription.unsubscribe()
    }

    public close(): void {
        this.closeEvent.emit()
    }

    public handlePerformanceSubscription(performances: Performance[]) {
        this.loading = false
        this.performances = performances
    }

    public handlePerformanceErrorSubscription(_error: Error) {
        this.loading = false
        this.errorText = 'Error fetching player info'
    }
}
