import { Component, Input, OnDestroy, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Subscription } from 'rxjs'
import { Entry } from 'src/app/entry/resources/entry'
import { PlayerService } from 'src/app/player/player.service'
import { Player } from 'src/app/player/resources/player'
import { User } from 'src/app/shared/models/User'
import { AuthService } from 'src/app/shared/services/auth.service'
import { EntryService } from '../entry/entry.service'
import { EntryViewModel } from '../entry/view-models/entry.viewmodel'
import { NotificationService } from '../notifications/notification-service'

@Component({
    selector: 'app-pick-team-new',
    templateUrl: './pick-team-new.component.html',
    styleUrls: ['./pick-team-new.component.scss']
})
export class PickTeamNewComponent implements OnDestroy {
    @Input() incompleteEntry: EntryViewModel = undefined

    public entrySubscription: Subscription = undefined
    public pickedPlayerSubscription: Subscription = undefined

    public user: User
    public entry: Entry = new Entry

    public showPlayerPicker: boolean = false
    public errorText: string = ''

    public loading: boolean

    constructor(
        private authService: AuthService,
        private playerService: PlayerService,
        private entryService: EntryService, private router: Router,
        private notificationService: NotificationService) {
        this.user = authService.getUser()

        this.pickedPlayerSubscription = this.playerService.pickedPlayer$.subscribe(x => this.handlePlayerSubscription(x))
    }

    public ngOnDestroy(): void {
        this.pickedPlayerSubscription.unsubscribe()
    }

    public handlePlayerSubscription(player: Player) { //why cant we move all this into services, too much logic in components
        if (player) {
            this.entry.players.push(player)
        }
    }

    public emptyPlayerClickedEventHandler(show: boolean): void {
        this.showPlayerPicker = show
    }

    public removePlayerClickEventHandler(player: Player): void {
        const index = this.entry.players.indexOf(player)

        if (index > -1) {
            this.entry.players.splice(index, 1)
        }
    }

    public saveTeam(): void {
        this.notificationService.addError('')

        this.entry.players.forEach(player => {
            player.captain = false
            player.viceCaptain = false
            player.substitute = false
        })

        const captIndex = this.entry.players.indexOf(this.entry.players.find(x => x.position === 4))
        const viceCaptIndex = this.entry.players.indexOf(this.entry.players.find(x => x.position === 3))

        this.entry.players[captIndex].captain = true
        this.entry.players[viceCaptIndex].viceCaptain = true

        for (let i = 1; i < 5; i++) {
            const pos = this.entry.players.filter(x => x.position === i && (!x.captain || !x.viceCaptain))
            const index = this.entry.players.indexOf(pos[pos.length-1])
            this.entry.players[index].substitute = true
            this.entry.players[index].squadPosition = i + 11
        }

        let squadPos = 1
        this.entry.players.forEach(p => {
            if (!p.substitute) {
                p.squadPosition = squadPos++
            }
        })

        this.entry.id = this.incompleteEntry.id
        this.loading = true

        this.entryService.update(this.user.id, this.entry).subscribe(() => {
            this.loading = false
            this.router.navigate(['/home'])
        }, () => {
            this.notificationService.addError('Failed to save team')
            this.loading = false
        })

    }
}
