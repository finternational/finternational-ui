import { ComponentFixture, TestBed } from '@angular/core/testing'

import { PickTeamNewComponent } from './pick-team-new.component'

describe('PickTeamNewComponent', () => {
    let component: PickTeamNewComponent
    let fixture: ComponentFixture<PickTeamNewComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ PickTeamNewComponent ]
        })
            .compileComponents()
    })

    beforeEach(() => {
        fixture = TestBed.createComponent(PickTeamNewComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
