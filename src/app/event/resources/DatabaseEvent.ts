export enum DatabaseEvent {
    GoalkeeperGoal = 3,
    DefenderGoal = 4,
    MidfielderGoal = 5,
    AttackerGoal = 6,
    Assist = 7,
    Saves = 11,
    PenaltySave = 12,
    PenaltyMiss = 13,
    YellowCard = 16,
    RedCard = 17,
    OwnGoal = 18
}