export enum UIEvent{
    Goal = 'Goals',
    OwnGoal = 'Own Goals',
    Assist = 'Assists',
    Saves = 'Saves',
    PenaltySave = 'Penalty Saves',
    PenaltyMiss = 'Penalty Misses',
    RedCard = 'Red Cards',
    YellowCard = 'Yellow Cards'
}