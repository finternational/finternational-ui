import { Component, Input, OnInit } from '@angular/core'
import { GameEvent } from 'src/app/games/resources/game-info'
import { DatabaseEvent } from './resources/DatabaseEvent'
import { UIEvent } from './resources/UIEvent'

@Component({
    selector: 'app-event',
    templateUrl: './event.component.html',
    styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

    @Input() event: string
    @Input() events: GameEvent[]
    @Input() homeId: number
    @Input() awayId: number

    public displayedEvents: GameEvent[]
    public homeEvents: GameEvent[]
    public awayEvents: GameEvent[]

    constructor() { }

    ngOnInit(): void {
        this.displayedEvents = []

        //move this / refactor it, could be a function?
        switch (this.event) {
        case UIEvent.Goal:
            this.displayedEvents = this.events.filter(x => x.eventType === DatabaseEvent.GoalkeeperGoal
                    || x.eventType === DatabaseEvent.DefenderGoal
                    || x.eventType === DatabaseEvent.MidfielderGoal
                    || x.eventType === DatabaseEvent.AttackerGoal)
            break
        case UIEvent.OwnGoal:
            this.displayedEvents = this.events.filter(x => x.eventType === DatabaseEvent.OwnGoal)
            break
        case UIEvent.Assist:
            this.displayedEvents = this.events.filter(x => x.eventType === DatabaseEvent.Assist)
            break
        case UIEvent.Saves:
            this.displayedEvents = this.events.filter(x => x.eventType === DatabaseEvent.Saves)
            break
        case UIEvent.PenaltySave:
            this.displayedEvents = this.events.filter(x => x.eventType === DatabaseEvent.PenaltySave)
            break
        case UIEvent.PenaltyMiss:
            this.displayedEvents = this.events.filter(x => x.eventType === DatabaseEvent.PenaltyMiss)
            break
        case UIEvent.RedCard:
            this.displayedEvents = this.events.filter(x => x.eventType === DatabaseEvent.RedCard)
            break
        case UIEvent.YellowCard:
            this.displayedEvents = this.events.filter(x => x.eventType === DatabaseEvent.YellowCard)
            break
        default:
            this.displayedEvents = []
        }

        this.homeEvents = this.displayedEvents
            .filter((value, index, self) =>
                index === self.findIndex((t) => (
                    t.playerLastName === value.playerLastName && t.eventType === value.eventType && t.countryId === this.homeId
                )))
        this.awayEvents = this.displayedEvents
            .filter((value, index, self) =>
                index === self.findIndex((t) => (
                    t.playerLastName === value.playerLastName && t.eventType === value.eventType && t.countryId === this.awayId
                )))
    }

    public getEventCount(playerName: string, countryId: number, eventType: number) {
        return this.displayedEvents.filter(x => x.countryId === countryId && x.playerLastName === playerName && x.eventType === eventType).length
    }

    get eventIsSaves() {
        return this.event === UIEvent.Saves
    }
}