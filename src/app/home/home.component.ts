import { Component, OnDestroy, OnInit } from '@angular/core'
import { Title } from '@angular/platform-browser'
import { Subscription } from 'rxjs'
import { HomeService } from 'src/app/home/service/home.service'
import { ServiceItem } from 'src/app/shared/models/service-item'
import { Gameweek } from '../gameweek/models/gameweek'
import { User } from '../shared/models/User'
import { AuthService } from '../shared/services/auth.service'

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
    public user: User
    public nextGameweek: Gameweek
    public loading: boolean
    public gameweekSubscription: Subscription = undefined

    public gameweeks!: ServiceItem<Gameweek[]>

    constructor(private title: Title, private authService: AuthService, private homeService: HomeService) {
        this.user = this.authService.getUser()
        this.gameweekSubscription = this.homeService.gameweeks$.subscribe(x => this.handleGameweekSubscription(x))
    }

    public ngOnInit(): void {
        this.title.setTitle('Finternational')
        this.loading = true

        this.homeService.loadGameweeks()
    }
    public ngOnDestroy(): void {
        this.gameweekSubscription.unsubscribe()
    }

    public handleGameweekSubscription(item: ServiceItem<Gameweek[]>): void {
        if (item !== undefined) {

            if (!item.error) {
                this.nextGameweek = item.data.find(x => x.next)
            }

            this.gameweeks = item
            this.loading = false
        }
    }
}
