import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core'
import { Subscription } from 'rxjs'
import { Entry } from '../entry/resources/entry'
import { NotificationService } from '../notifications/notification-service'
import { PlayerService } from '../player/player.service'
import { Player } from '../player/resources/player'
import { PlayerResponse } from '../player/resources/player-response'

@Component({
    selector: 'app-player-picker',
    templateUrl: './player-picker.component.html',
    styleUrls: ['./player-picker.component.scss']
})
export class PlayerPickerComponent implements OnDestroy {
    @Input() entry: Entry = undefined

    public searchFilter: number = 0
    public searchText: string = ''
    public players: PlayerResponse[] = []
    public searchPlayers: PlayerResponse[] = []

    public playersSubscription: Subscription = undefined
    public searchPositionSubscription: Subscription = undefined

    constructor(private playerService: PlayerService, private notificationService: NotificationService) {
        this.playersSubscription = this.playerService.players$.subscribe(x => this.handlePlayersSubscription(x))
        this.searchPositionSubscription = this.playerService.searchPosition$.subscribe(x => this.handleSearchPositionSubscription(x))

        this.playerService.load()
    }

    public ngOnDestroy(): void {
        this.playersSubscription.unsubscribe()
        this.searchPositionSubscription.unsubscribe()
    }


    public handleSearchPositionSubscription(position: number) {
        this.searchFilter = position
    }

    public handlePlayersSubscription(players: PlayerResponse[]) {
        if (players && this.entry) {
            this.players = players
            this.players = players.filter(x => x.position === this.searchFilter)

            const ids = this.entry.players.map(x => x.id)

            this.players = this.players.filter(x => !ids.includes(x.id))
            this.searchPlayers = this.players
        }
    }

    public playerSelected(player: PlayerResponse) {
        this.notificationService.addError('')

        if (this.entry.players.filter(x => x.countryId === player.country.id).length === 3) {
            this.notificationService.addError(`You already have 3 players from ${player.country.name}`)
        } else {
            this.playerService.publishPlayer(new Player(player.id, player.lastName, player.country.name, player.country.id, player.position, 0))
        }
    }

    public searchChanged() {
        if (this.players) {
            if (this.searchText === '') {
                this.searchPlayers = this.players
            } else {
                this.searchPlayers = this.players.filter(
                    x => x.lastName.toLowerCase().includes(this.searchText.toLowerCase()))
            }
        }
    }
}
