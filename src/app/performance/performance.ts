export class Performance {
    gameweek: number
    points: number
    playerTeamScore: number
    oppositionTeamScore: number
    oppositionName: string
    goals: number
    assists: number
    cleanSheet: boolean
    saves: number
    penaltySaves: number
    penaltiesWon: number
    penaltyMisses: number
    conceded: number
    yellowCards: number
    redCards: number
    minutes: number
    ownGoals: number
}