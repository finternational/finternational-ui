import { Injectable } from '@angular/core'
import { Subject } from 'rxjs'
import { ApiService } from '../shared/services/api.service'
import config from 'src/config/config.json'
import { Error } from '../error/error'
import { Performance } from './performance'

@Injectable()
export class PerformanceService {
    public performance$ = new Subject<Performance[]>()
    public error$ = new Subject<Error>()
    constructor(private apiService: ApiService) {}

    load(playerId: number): void {
        this.apiService.get<Performance[]>(`${config.fantasyApiUrl}/players/${playerId}/performances`).subscribe((response) => {
            this.performance$.next(response)
        }, (error) =>{
            this.error$.next(error.status)
        })
    }
}