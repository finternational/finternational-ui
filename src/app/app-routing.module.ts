import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { InfoComponent } from './info/info.component'
import { HomeComponent } from './home/home.component'
import { MsalGuard } from '@azure/msal-angular'
import { PickTeamComponent } from 'src/app/team/pick-team/pick-team.component'
import { LeagueExpandedComponent } from './league-expanded/league-expanded.component'
import { NotFoundComponent } from './not-found/not-found.component'
import { PointsComponent } from './points/points.component'
import { GamesComponent } from './games/games.component'
import { LeagueComponent } from './league/league.component'

/**
 * MSAL Angular can protect routes in your application
 * using MsalGuard. For more info, visit:
 * https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-angular/docs/v2-docs/initialization.md#secure-the-routes-in-your-application
 */
const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [
            MsalGuard
        ]
    },
    {
        path: 'team/pick',
        component: PickTeamComponent,
        canActivate: [
            MsalGuard
        ]
    },
    {
        path: 'league/:id/gameweek/:gameweekId',
        component: LeagueExpandedComponent,
        canActivate: [
            MsalGuard
        ]
    },
    {
        path: 'league',
        component: LeagueComponent,
        canActivate: [
            MsalGuard
        ]
    },
    {
        path: 'entry/:entryId/gameweek/:gameweekId',
        component: PointsComponent,
        canActivate: [
            MsalGuard
        ]
    },
    {
        path: 'games/gameweek/:gameweekId',
        component: GamesComponent,
        canActivate: [
            MsalGuard
        ]
    },
    {
        path: '',
        component: InfoComponent
    },
    {
        path: '**',
        pathMatch: 'full',
        component: NotFoundComponent
    }
]

const isIframe = window !== window.parent && !window.opener

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        useHash: false,
        // Don't perform initial navigation in iframes
        initialNavigation: !isIframe ? 'enabled' : 'disabled'
    })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
