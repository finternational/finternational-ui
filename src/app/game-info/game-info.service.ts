import { Injectable, OnInit } from '@angular/core'
import { Subject } from 'rxjs'
import { ApiService } from '../shared/services/api.service'
import config from 'src/config/config.json'
import { Error } from 'src/app/error/error'
import { GameInfo, GamePlayer } from '../games/resources/game-info'
import { ManagerPlayersViewModel, OwnedPlayerViewModel } from 'src/app/games/resources/manager-players.viewmodel'

@Injectable()
export class GameInfoService {
    public gameInfo$ = new Subject<GameInfo>()
    public error$ = new Subject<Error>()

    constructor(private apiService: ApiService) {}

    public load(gameId: number): void {
        this.apiService.get<GameInfo>(`${config.fantasyApiUrl}/gameinfo/game/${gameId}`).subscribe((response) => {
            this.gameInfo$.next(response)
        }, (error) =>{
            this.error$.next(error.status)
        })
    }

    public buildManagerPlayersViewModelCollection(players: GamePlayer[], homeId: number, _awayId: number): ManagerPlayersViewModel[] {
        const managerPlayersCollection: ManagerPlayersViewModel[] = []

        players.forEach(player => {
            const ownedPlayer = new OwnedPlayerViewModel(player.playerSecondName, player.substitute, player.captain, player.points)
            const home = player.countryId === homeId

            const index = managerPlayersCollection.findIndex(x => x.managerId === player.teamId)

            if (index < 0) {
                const managerPlayerViewModel = new ManagerPlayersViewModel(player.managerFirstName + ' ' + player.managerSecondName, player.teamId)

                if (home) {
                    managerPlayerViewModel.homePlayers.push(ownedPlayer)
                } else {
                    managerPlayerViewModel.awayPlayers.push(ownedPlayer)
                }

                managerPlayersCollection.push(managerPlayerViewModel)
            } else {
                if (home) {
                    managerPlayersCollection[index].homePlayers.push(ownedPlayer)
                } else {
                    managerPlayersCollection[index].awayPlayers.push(ownedPlayer)
                }
            }
        })

        return managerPlayersCollection
    }
}