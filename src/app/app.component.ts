import { Component } from '@angular/core'
import { AuthService } from 'src/app/shared/services/auth.service'

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    isIframe: boolean = false
    authenticated: boolean = false

    constructor(private authService: AuthService) {
        this.authenticated = this.authService.authenticated
    }
}