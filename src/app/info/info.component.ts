import { Component, OnInit } from '@angular/core'
import { Title } from '@angular/platform-browser'
import { Router } from '@angular/router'
import { AuthService } from 'src/app/shared/services/auth.service'

@Component({
    selector: 'app-info',
    templateUrl: './info.component.html',
    styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {
    constructor(private title: Title, private authService: AuthService, private router: Router) { }

    ngOnInit(): void {
        this.title.setTitle('Finternational')
        if (this.authService.authenticated) {
            this.router.navigate(['/home'])
        }
    }

    logIn(): void {
        this.authService.login()
    }

    signUp(): void {
        this.authService.signUp()
    }
}