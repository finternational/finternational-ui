import { Component, OnDestroy, OnInit } from '@angular/core'
import { Title } from '@angular/platform-browser'
import { ActivatedRoute, Router } from '@angular/router'
import { Subscription } from 'rxjs'
import { GameViewModel } from 'src/app/games/view-models/game.viewmodel'
import { GameweekViewModel } from 'src/app/gameweek/view-models/gameweek.viewmodel'
import { ServiceItem } from 'src/app/shared/models/service-item'
import { Error } from '../error/error'
import { GameInfoService } from '../game-info/game-info.service'
import { Gameweek } from '../gameweek/models/gameweek'
import { GameService } from './games.service'
import { GameInfo } from './resources/game-info'

@Component({
    selector: 'app-games',
    templateUrl: './games.component.html',
    styleUrls: ['./games.component.scss']
})
export class GamesComponent implements OnInit, OnDestroy {
    public gameweekSubscription: Subscription = undefined
    public gameSubscription: Subscription = undefined

    public gameInfoSubscription: Subscription
    public gameInfoErrorSubscription: Subscription

    public nextGameweek: Gameweek
    public currentGameweek: Gameweek

    public loading: boolean
    public noUpcomingGames: boolean

    public gameweeks: ServiceItem<GameweekViewModel[]>
    public games: ServiceItem<GameViewModel[]>

    public now: number
    public todaysGames: boolean
    public statsChecked: boolean

    public gameShownId: number
    public gameweekId: number

    constructor(
        private title: Title,
        private gameService: GameService,
        private gameInfoService: GameInfoService,
        private activatedRoute: ActivatedRoute,
        private router: Router) {
        this.gameweekSubscription = this.gameService.gameweeks$.subscribe(x => this.handleGameweekSubscription(x))
        this.gameSubscription = this.gameService.games$.subscribe(x => this.handleGameSubscription(x))
        this.gameInfoSubscription = this.gameInfoService.gameInfo$.subscribe(x => this.handleGameInfoSubscription(x))
        this.gameInfoErrorSubscription = this.gameInfoService.error$.subscribe(x => this.handleGameInfoErrorSubscription(x))
    }

    public ngOnDestroy(): void {
        this.gameweekSubscription.unsubscribe()
        this.gameSubscription.unsubscribe()

        this.gameInfoSubscription.unsubscribe()
        this.gameInfoErrorSubscription.unsubscribe()
    }

    public ngOnInit(): void {
        this.title.setTitle('Games')
        this.gameweekId = parseInt(this.activatedRoute.snapshot.paramMap.get('gameweekId'))
        this.gameShownId = undefined
        this.loading = true
        this.noUpcomingGames = false

        this.todaysGames = false
        this.statsChecked = true

        this.now = Date.now()

        this.gameService.loadGames(this.gameweekId)
    }

    public handleGameweekSubscription(item: ServiceItem<GameweekViewModel[]>): void {
        if (item !== undefined) {
            this.gameweeks = item

            if (!item.error) {
                this.nextGameweek = this.gameweeks.data.find(x => x.next)
                this.currentGameweek = this.gameweeks.data.find(x => x.current)
            }
        }
    }

    public handleGameSubscription(games: ServiceItem<GameViewModel[]>): void {
        if (games !== undefined) {
            if (!games.error) {
                if (!this.todaysGames) {
                    this.title.setTitle(`Games - GW${this.gameweekId}`)
                }
            }

            this.games = games
            this.loading = false
        }
    }

    public handleGameInfoSubscription(gameInfo: GameInfo) {
        const index = this.games.data.findIndex(x => x.id === gameInfo.gameId)
        const game = this.games.data[index]

        this.games.data[index].gameInfo = gameInfo
        this.games.data[index].homeGoals = gameInfo.homeGoals
        this.games.data[index].awayGoals = gameInfo.awayGoals

        const players = this.gameInfoService.buildManagerPlayersViewModelCollection(gameInfo.ownedPlayers, game.homeId, game.awayId)
        this.games.data[index].gameInfo.ownedPlayersViewModels = players

        this.loading = false
    }

    public handleGameInfoErrorSubscription(error: Error): void {
        switch (error) {
        default:
            this.games.error = true //todo change this
            break
        }

        this.loading = false
    }

    public todaysGamesSelectedEvent() {
        this.title.setTitle('Games - Today')
        this.loading = true
        this.todaysGames = true

        this.gameService.loadForToday()
    }

    public gameweekGamesSelectedEvent() {
        this.loading = true
        this.todaysGames = false

        this.gameService.loadGames(this.gameweekId)
    }

    public playersSelectedEvent() {
        this.statsChecked = false
    }

    public statsSelectedEvent() {
        this.statsChecked = true
    }

    public gameClicked(gameId: number): void {
        this.loading = true

        if (this.gameShownId === gameId) {
            this.gameShownId = undefined
        } else {
            this.gameShownId = gameId
        }

        const index = this.games.data.findIndex(x => x.id === gameId)

        if (this.games.data[index].gameInfo === undefined) {
            this.gameInfoService.load(gameId)
        } else {
            this.loading = false
        }
    }

    public kickedOff(kickOff: Date) {
        return new Date(Date.now()) > new Date(kickOff)
    }

    public getPointsUrl(managerId: string): string {
        const gameweek = this.currentGameweek?.id ?? this.nextGameweek.id

        return `/entry/${managerId}/gameweek/${gameweek}`
    }

    public navigateForwardBack(forward: boolean): void {
        this.loading = true

        if (forward) {
            this.gameweekId++
        } else {
            this.gameweekId--
        }

        this.router.navigate([`/games/gameweek/${this.gameweekId}`])
        this.gameService.loadGames(this.gameweekId)
    }

    get canNavigateBack(): boolean {
        return this.gameweekId > this.gameweeks.data.find(x => true)?.id
    }

    get canNavigateForward(): boolean {
        return this.gameweekId < this.gameweeks.data[this.gameweeks.data.length - 1]?.id
    }

    get titleText(): string {
        return this.todaysGames ? 'Today' : `GW${this.gameweekId}`
    }
}
