import { GameInfo } from './game-info'

export class Game {
    id: number
    homeId: number
    homeName: string
    homeGoals: number
    awayId: number
    awayName: string
    awayGoals: number
    kickOff: Date
    finished: boolean
    gameInfo: GameInfo
}