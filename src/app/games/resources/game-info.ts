import { ManagerPlayersViewModel } from 'src/app/games/resources/manager-players.viewmodel'

export class GameInfo {
    gameId: number
    homeGoals: number
    awayGoals: number
    ownedPlayers: GamePlayer[]
    gameEvents: GameEvent[]
    ownedPlayersViewModels: ManagerPlayersViewModel[]
}

export class GameEvent {
    eventType: number
    playerFirstName: string
    playerLastName: string
    countryId: number
}

export class GamePlayer {
    countryId: number
    playerFirstName: string
    playerSecondName: string
    managerFirstName: string
    managerSecondName: string
    teamId: string
    substitute: boolean
    captain: boolean
    points: number
}