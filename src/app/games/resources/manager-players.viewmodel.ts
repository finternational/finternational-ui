export class ManagerPlayersViewModel {
    managerName: string
    managerId: string
    homePlayers: OwnedPlayerViewModel[]
    awayPlayers: OwnedPlayerViewModel[]

    constructor(managerName: string, managerId: string) {
        this.managerName = managerName
        this.managerId = managerId
        this.homePlayers = []
        this.awayPlayers = []
    }
}

export class OwnedPlayerViewModel {
    playerName: string
    substitute: boolean
    captain: boolean
    points: number

    constructor(playerName: string, substitute: boolean, captain: boolean, points: number) {
        this.playerName = playerName
        this.substitute = substitute
        this.captain = captain
        this.points = points
    }
}