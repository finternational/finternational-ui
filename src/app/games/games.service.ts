import { Injectable, OnInit } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { ApiService } from '../shared/services/api.service'
import config from 'src/config/config.json'
import { Game } from './resources/game'
import { GameweekService } from 'src/app/gameweek/gameweek.service'
import { GameweekViewModel } from 'src/app/gameweek/view-models/gameweek.viewmodel'
import { ServiceItem } from 'src/app/shared/models/service-item'
import { GameViewModel } from 'src/app/games/view-models/game.viewmodel'

@Injectable()
export class GameService {
    public gameweeks$ = new BehaviorSubject<ServiceItem<GameweekViewModel[]>>(undefined)
    public games$ = new BehaviorSubject<ServiceItem<GameViewModel[]>>(undefined)

    constructor(
        private gameweekService: GameweekService,
        private apiService: ApiService) {
    }

    public loadGames(gameweekId: number): void {
        this.gameweekService.get()
            .subscribe((response) => {
                this.gameweeks$.next(new ServiceItem(response, false, undefined))
                this.loadGamesInternal(gameweekId)
            },
            (error: number) => {
                this.gameweeks$.next(new ServiceItem(undefined, true, error))
                this.games$.next(new ServiceItem(undefined, true, error))
            })
    }

    public loadForToday() {
        this.apiService.getErrorHandling<Game[]>(`${config.fantasyApiUrl}/games/today`).subscribe((response) => {
            this.loadForTodayComplete(response)
        }, (error) =>{
            this.games$.next(new ServiceItem(undefined, true, error))
        })
    }

    private loadGamesInternal(gameweekId: number): void {
        this.apiService.getErrorHandling<Game[]>(`${config.fantasyApiUrl}/games/gameweek/${gameweekId}`)
            .subscribe((response) => {
                this.loadGamesForCorrectGameweekComplete(response)
            }, (error) =>{
                this.games$.next(new ServiceItem(undefined, true, error))
            })
    }

    private loadGamesForCorrectGameweekComplete(data: Game[]): void {
        const viewModels = data.map(x => new GameViewModel(x.id, x.homeId, x.homeName, x.homeGoals, x.awayId, x.awayName, x.awayGoals, x.kickOff, x.finished, x.gameInfo))
        this.games$.next(new ServiceItem(viewModels, false, undefined))
    }

    private loadForTodayComplete(data: Game[]): void {
        const viewModels = data.map(x => new GameViewModel(x.id, x.homeId, x.homeName, x.homeGoals, x.awayId, x.awayName, x.awayGoals, x.kickOff, x.finished, x.gameInfo))
        this.games$.next(new ServiceItem(viewModels, false, undefined))
    }
}