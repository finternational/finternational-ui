import { GameInfo } from '../resources/game-info'

export class GameViewModel {
    id: number
    homeId: number
    homeName: string
    homeGoals: number
    awayId: number
    awayName: string
    awayGoals: number
    kickOff: Date
    finished: boolean
    gameInfo: GameInfo

    constructor(
        id: number,
        homeId: number,
        homeName: string,
        homeGoals: number,
        awayId: number,
        awayName: string,
        awayGoals: number,
        kickOff: Date,
        finished: boolean,
        gameInfo: GameInfo) {
        this.id = id
        this.homeId = homeId
        this.homeName = homeName
        this.homeGoals = homeGoals
        this.awayId= awayId
        this.awayName = awayName
        this.awayGoals = awayGoals
        this.kickOff = kickOff
        this.finished = finished
        this.gameInfo = gameInfo
    }
}