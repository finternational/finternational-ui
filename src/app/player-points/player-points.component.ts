import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { DeviceDetectorService } from 'ngx-device-detector'
import { Player } from '../player/resources/player'

@Component({
    selector: 'app-player-points',
    templateUrl: './player-points.component.html',
    styleUrls: ['./player-points.component.scss']
})
export class PlayerPointsComponent implements OnInit {
    @Input() player: Player = undefined
    @Output() showPointsInfoEvent: EventEmitter<Player> = new EventEmitter()

    public kitPath: string = '/assets/50.png'
    public isMobile: boolean = undefined

    constructor(private deviceService: DeviceDetectorService) { }

    ngOnInit(): void {
        this.isMobile = this.deviceService.isMobile()

        if (this.isMobile) {
            this.kitPath = `/assets/${this.player.countryName}50.png`
        } else {
            this.kitPath = `/assets/${this.player.countryName}.png`
        }
    }

    public pointsClicked(): void {
        this.showPointsInfoEvent.emit(this.player)
    }
}
