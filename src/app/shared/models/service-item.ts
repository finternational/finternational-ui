export class ServiceItem<T> {
    data: T
    error: boolean
    errorCode: number

    constructor(data: T, error: boolean, errorCode: number) {
        this.data = data
        this.error = error
        this.errorCode = errorCode
    }
}