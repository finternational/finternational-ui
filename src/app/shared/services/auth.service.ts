import { Inject, Injectable } from '@angular/core'
import { MSAL_GUARD_CONFIG, MsalBroadcastService, MsalGuardConfiguration, MsalService } from '@azure/msal-angular'
import { AuthenticationResult, InteractionType, IPublicClientApplication, PopupRequest, RedirectRequest } from '@azure/msal-browser'
import { b2cPolicies } from '../../auth-config'
import { User } from '../models/User'

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    public instance: IPublicClientApplication

    constructor(
    @Inject(MSAL_GUARD_CONFIG) private msalGuardConfig: MsalGuardConfiguration,
    private msalService: MsalService) {
        this.instance = this.msalService.instance
    }

    get authenticated(): boolean {
        return this.msalService.instance.getAllAccounts().length > 0
    }

    public getUser(): User {
        const accounts = this.msalService.instance.getAllAccounts()

        if (accounts.length === 0) {
            return { id: '', firstName: '' } //could redirect here?
        }

        const claims = accounts[0].idTokenClaims as any

        return { id: claims.sub, firstName: claims.given_name }
    }

    login(userFlowRequest?: RedirectRequest | PopupRequest) {
        if (this.msalGuardConfig.interactionType === InteractionType.Popup) {
            if (this.msalGuardConfig.authRequest) {
                this.msalService.loginPopup({ ...this.msalGuardConfig.authRequest, ...userFlowRequest } as PopupRequest)
                    .subscribe((response: AuthenticationResult) => {
                        this.msalService.instance.setActiveAccount(response.account)
                    })
            } else {
                this.msalService.loginPopup(userFlowRequest)
                    .subscribe((response: AuthenticationResult) => {
                        this.msalService.instance.setActiveAccount(response.account)
                    })
            }
        } else {
            if (this.msalGuardConfig.authRequest) {
                this.msalService.loginRedirect({ ...this.msalGuardConfig.authRequest, ...userFlowRequest } as RedirectRequest)
            } else {
                this.msalService.loginRedirect(userFlowRequest)
            }
        }
    }

    async signUp() {
        let editProfileFlowRequest = {
            scopes: ['openid'],
            authority: b2cPolicies.authorities.signUp.authority
        }

        this.login(editProfileFlowRequest)

        editProfileFlowRequest = {
            scopes: ['openid'],
            authority: b2cPolicies.authorities.signUpSignIn.authority
        }

        await this.instance.acquireTokenSilent(editProfileFlowRequest) //temp fix, how do we fully stop the login request having wrong b2c once?
    }

    logout() {
        this.msalService.logout()
    }
}