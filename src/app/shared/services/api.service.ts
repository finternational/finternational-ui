import { Injectable } from '@angular/core'
import { catchError, Observable, throwError } from 'rxjs'
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http'
import { QueryParameter } from '../resources/queryParameter'

@Injectable()
export class ApiService {

    constructor(private httpClient: HttpClient) {}

    public get<TResponse>(url: string, parameters?: QueryParameter[]): Observable<TResponse> {
        const params = new HttpParams()

        if (parameters !== undefined) {
            parameters.forEach(parameter => {
                params.append(parameter.name, parameter.value)
            })
        }

        return this.httpClient.get<TResponse>(url)
    }

    public getErrorHandling<TResponse>(url: string, parameters?: QueryParameter[]): Observable<TResponse> {
        const params = new HttpParams()

        if (parameters !== undefined) {
            parameters.forEach(parameter => {
                params.append(parameter.name, parameter.value)
            })
        }

        return this.httpClient.get<TResponse>(url)
            .pipe(catchError((error: HttpErrorResponse) => {
                return throwError(error.status)
            }))
    }

    public patch<TBody>(url: string, body: TBody): Observable<any> {
        return this.httpClient.patch(url, body)
    }

    public post<TBody, TResponse>(url: string, body: TBody): Observable<TResponse> {
        return this.httpClient.post<TResponse>(url, body)
            .pipe(catchError((error: HttpErrorResponse) => {
                return throwError(error.status)
            }))
    }
}