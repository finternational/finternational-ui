import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { MsalGuardConfiguration, MsalModule, MSAL_INSTANCE, MSAL_GUARD_CONFIG, MsalService, MsalGuard, MsalBroadcastService, MsalRedirectComponent, MsalInterceptorConfiguration, MSAL_INTERCEPTOR_CONFIG, MsalInterceptor } from '@azure/msal-angular'
import { IPublicClientApplication, PublicClientApplication, InteractionType } from '@azure/msal-browser'
import { msalConfig } from '../auth-config'
import { ApiService } from './services/api.service'
import { AuthService } from './services/auth.service'
import config from 'src/config/config.json'

/**
 * Here we pass the configuration parameters to create an MSAL instance.
 * For more info, visit: https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-angular/docs/v2-docs/configuration.md
 */
export function MSALInstanceFactory(): IPublicClientApplication {
    return new PublicClientApplication(msalConfig)
}

/**
 * Set your default interaction type for MSALGuard here. If you have any
 * additional scopes you want the user to consent upon login, add them here as well.
 */
export function MSALGuardConfigFactory(): MsalGuardConfiguration {
    return {
        interactionType: InteractionType.Redirect,
    }
}

export function MSALInterceptorConfigFactory(): MsalInterceptorConfiguration {
    const protectedResourceMap = new Map<string, Array<string>>()

    protectedResourceMap.set(config.fantasyApiUrl, config.B2C.protectedResources.fantasyApi.scopes)

    return {
        interactionType: InteractionType.Redirect,
        protectedResourceMap
    }
}


@NgModule({
    declarations: [
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        MsalModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: MsalInterceptor,
            multi: true
        },
        {
            provide: MSAL_INSTANCE,
            useFactory: MSALInstanceFactory
        },
        {
            provide: MSAL_GUARD_CONFIG,
            useFactory: MSALGuardConfigFactory
        },
        {
            provide: MSAL_INTERCEPTOR_CONFIG,
            useFactory: MSALInterceptorConfigFactory
        },
        MsalService,
        MsalBroadcastService,
        AuthService,
        ApiService
    ]
})
export class SharedModule { }