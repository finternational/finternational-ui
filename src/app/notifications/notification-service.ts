import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs'

@Injectable()
export class NotificationService {
    public errorText$ = new BehaviorSubject<string>('')

    addError(text: string): void {
        this.errorText$.next(text)
    }
}